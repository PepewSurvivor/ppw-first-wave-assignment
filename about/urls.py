from django.urls import path
from about.views import *

urlpatterns = [
    path('', about, name='about'),
    path('new_testimoni/', new_testimoni, name = 'new_testimoni'),
]
