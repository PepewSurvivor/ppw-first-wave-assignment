from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import about.views as views
from main.models import *
from django.contrib.auth.models import User

class AboutUnitTest(TestCase):

    def test_about_url_is_exist_user_has_not_login(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_url_is_exist_user_has_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_use_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, views.about)

    def test_about_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_about_page_is_complete(self):
        request = HttpRequest()
        response = views.about(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Cari tahu kondisi sekitar anda', html_response)

    def test_about_not_login_not_display_form(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Tambahkan testimoni anda', html_response)

    def test_about_has_login_display_form(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn('Tambahkan testimoni anda', html_response)

    def test_about_using_new_testimoni_func(self):
        found = resolve('/about/new_testimoni/')
        self.assertEqual(found.func, views.new_testimoni)

    def test_succest_post_testimony_and_rendered(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        
        name = 'testuser'
        testimoni = 'tes'
        response_post = c.post('/about/new_testimoni/',{'name' : name, 'testimoni':testimoni})
        response = c.get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)
        self.assertIn(testimoni, html_response)
    
# Create your tests here.
