from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from main.models import Testimoni

response = {}
def about(request):
    response['testimony'] = Testimoni.objects.all()
    return render(request, 'about.html', response)

def new_testimoni(request):
    if request.method == 'POST':
        name = request.POST['name']
        testimoni = request.POST['testimoni']

        new_testimoni = Testimoni.objects.create(
            testimoni_name = name,
            testimoni_konten = testimoni,
        )
        new_testimoni.save()

        testimoni_object = Testimoni.objects.all().values('testimoni_name', 'testimoni_konten')
        testimoni_list = list(testimoni_object)

        return JsonResponse(testimoni_list, safe = False)
    else:
        return HttpResponseRedirect('/about/')
