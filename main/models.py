from django.db import models
from django.contrib.auth.models import User
from datetime import date

class Users(models.Model):
    full_name = models.CharField(max_length = 100)
    birth_date = models.DateField()
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length = 16)



class Donatur2(models.Model):
    donatur_name = models.CharField(max_length = 100)
    donatur_email = models.CharField(max_length = 100, default="example@gmail.com")
    donatur_showable = models.BooleanField(default = False)
    total_money = models.IntegerField(default = 0)
    program_name = models.CharField(max_length = 50, default="example")

class Program(models.Model):
    program_name = models.CharField(max_length = 50)
    donatur = models.ManyToManyField('Donatur2', blank=True)
    summary = models.TextField(max_length = 300)
    description = models.TextField(max_length = 600)
    photo = models.CharField(max_length = 100, default="https://files.catbox.moe/kil8y5.jpg")
    created_date = models.DateTimeField(auto_now_add = True)

    def get_all_donation(self):
        temp = 0
        for adonatur in self.donatur.all():
            temp += adonatur.total_money
        return temp

class News(models.Model):
    news_name = models.CharField(max_length = 50)
    program = models.ManyToManyField('Program', blank=True)
    summary = models.TextField(max_length = 300)
    description = models.TextField(max_length = 600)
    photo = models.CharField(max_length = 100, default="https://files.catbox.moe/kil8y5.jpg")
    created_date = models.DateTimeField(auto_now_add = True)

class Testimoni(models.Model):
    testimoni_name = models.CharField(max_length = 50)
    testimoni_konten = models.TextField(max_length = 300)
