from django.contrib import admin

from .models import Users, Donatur2, Program, News, Testimoni
admin.site.register(Users)
admin.site.register(Donatur2)
admin.site.register(Program)
admin.site.register(News)
admin.site.register(Testimoni)
