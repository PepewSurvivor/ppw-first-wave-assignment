from django.apps import apps
from django.test import TestCase
from main.apps import MainConfig
from .models import *
import pytz
from .views import *
from datetime import date
from django.http import Http404

class MainUnitTest(TestCase):
    def test_model_can_create_new_testimoni(self):
        # Creating a new user
        new_testimoni = Testimoni.objects.create(
            testimoni_name = "Clouddian Fazalmuttaqin", 
            testimoni_konten = "Website ini keren")

        # Retrieving all user
        counting_all_testimoni = Testimoni.objects.all().count()
        self.assertEqual(counting_all_testimoni, 1)

    def test_model_can_create_new_user(self):
        # Creating a new user
        new_user = Users.objects.create(
            full_name = "Clouddian Fazalmuttaqin", 
            birth_date = "1999-6-10",
            email = "clauddian9@gmail.com",
            password = "user1")

        # Retrieving all user
        counting_all_users = Users.objects.all().count()
        self.assertEqual(counting_all_users, 1)

    def test_model_can_create_new_donatur(self):
        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")
        new_program.save()

        new_donatur = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin", 
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")

        # Retrieving all donatur
        counting_all_donatur = Donatur2.objects.all().count()
        self.assertEqual(counting_all_donatur, 1)
        

    def test_model_can_create_new_program(self):
        # Creating a new program
        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        # add the donatur
        new_donatur1 = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin", 
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000)
        new_donatur2 = Donatur2.objects.create(
            donatur_name = "Arif Dermawan",
            donatur_email = "arif.dermawan@gmail.com",
            donatur_showable = True,
            total_money = 1000000)
        new_donatur1.save()
        new_donatur2.save()
        new_program.donatur.add(new_donatur1, new_donatur2)

        # Retrieving all program
        counting_all_program = Program.objects.all().count()
        self.assertEqual(counting_all_program, 1)

    def test_model_can_create_new_news(self):
        # Creating a new news
        new_news = News.objects.create(
            news_name = "berita banjir",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg"
        )

        # Creating a new program
        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa")

        # add the donatur to program
        new_donatur1 = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin", 
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000)
        new_donatur2 = Donatur2.objects.create(
            donatur_name = "Arif Dermawan", 
            donatur_email = "arif.dermawan@gmail.com",
            donatur_showable = True,
            total_money = 1000000)
        new_donatur1.save()
        new_donatur2.save()
        new_program.donatur.add(new_donatur1, new_donatur2)

        # add the program to news
        new_program.save()
        new_news.program.add(new_program)

        # Retrieving all news
        counting_all_news = News.objects.all().count()
        self.assertEqual(counting_all_news, 1)

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main')

    def test_invalid_url_raises_response_not_found(self):
        response = self.client.get('/error/')
        self.assertRaisesMessage(Http404, 'RevMatch404')

    def test_invalid_url_return_404(self):
        response = self.client.get('/error')
        self.assertEquals(response.status_code, 404)

    def test_404_handler_used_page_not_found_func(self):
        from PPW_First_Wave_Assignment.urls import handler404
        names = handler404.split('.')
        handler404 = __import__(handler404[:handler404.find('.')])
        for name in names[1:]:
            handler404 = handler404.__getattribute__(name)
        self.assertEquals(handler404, page_not_found)

    def test_404_handler_used_404_template(self):
        response = self.client.get('/error/')
        self.assertTemplateUsed(response, '404.html')

