from django.shortcuts import render, redirect

id_user_main = 1

def page_not_found(request, exception, template_name='404.html'):
    from django.http import HttpResponseNotFound
    from django.template import loader
    template = loader.get_template(template_name)
    context = {'request_path': request.path}
    body = template.render(context, request)
    return HttpResponseNotFound(body)
