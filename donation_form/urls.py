from django.urls import path
from donation_form.views import donation_form, donation_add

urlpatterns = [
    path('', donation_form, name = 'donation_form'),
    path('donation_add/', donation_add, name = 'donation_add'),
]