from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
import donation_form.views as views
from main.models import *

class Lab6UnitTest(TestCase):
    
    def test_donation_form_url_is_exist_has_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/donation_form/')
        self.assertEqual(response.status_code, 200)

    def test_donation_form_url_is_exist_not_login(self):
        response = Client().get('/donation_form/')
        self.assertEqual(response.status_code, 302)

    def test_donation_url_not_exist(self):
        response = Client().get('/error/')
        self.assertEqual(response.status_code, 404)

    def test_donation_add_url_is_exist(self):
        response = Client().get('/donation_form/donation_add/')
        self.assertEqual(response.status_code, 302)

    def test_donation_form_using_donation_form_func(self):
        found = resolve('/donation_form/')
        self.assertEqual(found.func, views.donation_form)

    def test_donation_form_using_donation_add_func(self):
        found = resolve('/donation_form/donation_add/')
        self.assertEqual(found.func, views.donation_add)

    def test_donation_form_using_donation_form_template(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')

        response = c.get('/donation_form/')
        self.assertTemplateUsed(response, 'donation_form.html')
    
    def test_donation_form_post_success(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')

        new_program = Program.objects.create(
            program_name = "Donasi Global",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa")

        response_post = Client().post(
            '/donation_form/donation_add/', 
            {'program_name': 'Donasi Global',
            'donatur_name': 'cloud',
            'is_hidden': True,
            'total_money': 100,})
        self.assertEqual(response_post.status_code, 200)


