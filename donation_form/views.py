from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from main.models import *

response = {}
def donation_form(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/register/")
    html = 'donation_form.html'
    response['donation_program'] = Program.objects.all()
    return render(request, html, response)

def donation_add(request):
    if request.method == 'POST':
        program_name = request.POST['program_name']
        program_object = Program.objects.get(program_name = program_name)

        donatur_showable = True
        if request.POST['is_hidden'] == "1": 
            donatur_showable = False

        donatur_email = "example@gmail.com"
        if not request.user.is_anonymous:
            donatur_email = request.user.email

        new_donatur = Donatur2(
            donatur_name = request.POST['donatur_name'],
            donatur_email = donatur_email,
            donatur_showable = donatur_showable,
            total_money = request.POST['total_money'],
            program_name = program_name,
        )
        new_donatur.save()

        program_object.donatur.add(new_donatur)
        program_object.save()
        return HttpResponse('')

    else:
        return HttpResponseRedirect('/donation_form/')