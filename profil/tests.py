from django.http import HttpRequest
from django.test import Client
from django.urls import resolve
from django.test import TestCase
import profil.views as views
from main.models import *

# Create your tests here.
class NewsUnitTest(TestCase):
	def test_news_page_url_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 302)

	def test_news_using_news_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, views.profil)

	# def test_list_program(self):
	# 	new_donatur1 = Donatur2.objects.create(
	# 		donatur_name = "Clouddian Fazalmuttaqin",
	# 		donatur_email = "clauddian9@gmail.com",
	# 		donatur_showable = True,
	# 		total_money = 1000000,
	# 		program_name = "banjir pekalongan")
	# 	new_donatur1.save()

	# 	user = User.objects.create(username='testuser')
	# 	user.set_password('12345')
	# 	user.save()

	# 	c = Client()
	# 	logged_in = c.login(username='testuser', password='12345')
	# 	response = c.get('/profile/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn("banjir pekalongan", html_response)
    
	# def test_news_using_news_template(self):
	# 	user = User.objects.create(username='testuser')
	# 	user.set_password('12345')
	# 	user.save()

	# 	c = Client()
	# 	logged_in = c.login(username='testuser', password='12345')
	# 	response = Client().get('/profile/')
	# 	self.assertTemplateUsed(response, 'profil.html')
