from django.urls import path
from profil.views import *

app_name = 'profil'
urlpatterns = [
	path('', profil, name = 'profil'),
    path('cek-history/', cek_history, name='cek-history'),
]