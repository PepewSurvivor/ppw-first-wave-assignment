from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core import serializers
from main.models import *
import json
# from django.contrib.auth.models import User

response = {}

def profil(request):
	if not request.user.is_authenticated:
		return HttpResponseRedirect("/register/")

	data = {}
	email = request.user.email
	print(email)
	username = request.user.username

	donasi = Donatur2.objects.all().filter(donatur_email=email)

	total_donasi = 0

	for i in donasi:
		total_donasi += i.total_money
	print(total_donasi)

	return render(request, 'profil.html', {'donasi' : donasi, 'username' : username, 'total_donasi':total_donasi})

def cek_history(request):
	email = request.user.email
	username = request.user.username

	data = serializers.serialize("json", Donatur2.objects.all().filter(donatur_email=email))
	json_now = json.dumps(json.loads(data))
	return HttpResponse(data, content_type='application/json')