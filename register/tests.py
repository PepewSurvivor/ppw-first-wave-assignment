from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from main.models import *
from .views import *

class RegisterUnitTest(TestCase):

    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_use_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_model_can_create_new_model(self):
        new_activity = Users.objects.create(full_name = 'Anditama', birth_date = '1998-07-17', email = 'andi17@gmail.com', password = 'anditama123' )

        counting_all_available_users = Users.objects.all().count()
        self.assertEqual(counting_all_available_users, 1)

    def test_form_validation_for_blank_items(self):
        form = Register_Form(data={'full_name': '', 'birth_date': '', 'email':'', 'password':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['full_name'],
            ["This field is required."]
        )

    def test_form_register_input_has_placeholder(self):
        form = Register_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_full_name">Nama:</label>', form.as_p())
        self.assertIn('<label for="id_birth_date">Tanggal lahir:</label>', form.as_p())
        self.assertIn('<label for="id_email">E-mail:</label>', form.as_p())
        
    def test_register_post_success_and_redirect(self):
        response_post = Client().post('/register/user_registration/', {'full_name': 'Anditama', 'birth_date': '1998-07-17', 'email': 'andi17@gmai.com', 'password':'anditama123', 'confirm_password':'anditama123'})
        
        self.assertEqual(response_post.status_code, 302)

    def test_register_post_is_not_valid(self):
        response_post = Client().post('/register/user_registration/', {'full_name': '', 'birth_date': '', 'email':'', 'password':'', 'confirm_password':''})
        self.assertEqual(response_post.status_code, 302)
        
    def test_register_password_is_different_from_confirm_and_redirect_again(self):
        response_post = Client().post('/register/user_registration/', {'full_name': 'Anditama', 'birth_date': '1998-07-17', 'email': 'andi17@gmai.com', 'password':'anditama123', 'confirm_password':'anditama'})
        self.assertEqual(response_post.status_code, 302)
        
# Create your tests here.
