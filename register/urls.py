from django.urls import path
from register.views import register, register_post

urlpatterns = [
    path('', register, name = 'register'),
    path('user_registration/', register_post, name = 'register_post')
]
