from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import *
from main.models import *
response = {}
def register(request):
    response['register_form'] = Register_Form
    return render(request, 'register.html', response)

def register_post(request):
    form = Register_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['full_name'] = request.POST['full_name']if request.POST['full_name']!="" else "Anonymous"
        response['birth_date'] = request.POST['birth_date']if request.POST['birth_date']!="" else "Anonymous"
        response['email'] = request.POST['email']if request.POST['email']!="" else "Anonymous"
        if (request.POST['password'] == request.POST['confirm_password']):
            response['password'] = request.POST['password']
            userbaru = Users(full_name = response['full_name'], birth_date = response['birth_date'], email = response['email'], password = response['password'])
            userbaru.save()
            return HttpResponseRedirect('/dashboard/')
        else:
            return HttpResponseRedirect('/register/')
    else:
        return HttpResponseRedirect('/register/')
# Create your views here.
