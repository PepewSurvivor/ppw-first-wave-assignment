from django import forms
from main.models import *

class DateInput(forms.DateInput):
    input_type = 'date'
class Conf_Pass(forms.Form):
    attrs = {
        'class': 'form-control',
        'oninput':'check(this)',
        }
class Register_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    full_name = forms.CharField(label = 'Nama', required = True, widget=forms.TextInput(attrs=attrs))
    birth_date = forms.DateField(label = 'Tanggal lahir', required = True, widget=DateInput(attrs=attrs))
    email = forms.EmailField(label = 'E-mail', required = True, widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label = 'Kata Sandi', required = False, max_length = 32, widget = forms.PasswordInput(attrs=attrs))
    confirm_password = forms.CharField(label = 'Konfirmasi Kata Sandi', required = False, widget = forms.PasswordInput(attrs={'class':'form-control','oninput':'check(this)'}))

    class Meta:
        model = Users
