## Anggota Kelompok:
1. Nama = Clouddian Fazalmuttaqin
   NPM  = 1706984543
2. Nama = Muhammad Arief Darmawan
   NPM  = 1706043746
3. Nama = Irfan Aziz Al Amin
   NPM  = 1706039364
4. Nama = Muhammad Rizky Anditama
   NPM  = 1706044080

## Status pipelines:

[![pipeline status](https://gitlab.com/PepewSurvivor/ppw-first-wave-assignment/badges/master/pipeline.svg)](https://gitlab.com/PepewSurvivor/ppw-first-wave-assignment/commits/master)

## Status code coverage:
[![coverage report](https://gitlab.com/PepewSurvivor/ppw-first-wave-assignment/badges/master/coverage.svg)](https://gitlab.com/PepewSurvivor/ppw-first-wave-assignment/commits/master)

## Link Heroku
https://first-wave.herokuapp.com/