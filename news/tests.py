from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from main.models import *
import news.views as views

# Create your tests here.
class NewsUnitTest(TestCase):
    def test_news_page_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_news_func(self):
        found = resolve('/news/')
        self.assertEqual(found.func, views.news)
    
    def test_news_using_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')

    def test_news_page_is_complete(self):
        request = HttpRequest()
        response = views.news(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Cari tahu kondisi sekitar anda', html_response)

    # def test_detail_page_url_is_exist(self):
    #     response = Client().get('/news/detail/<int:pk>')
    #     self.assertEqual(response.status_code, 200)

    # def test_detail_page_using_news_func(self):
    #     found = resolve('/news/detail/<int:pk>')
    #     self.assertEqual(found.func, views.detailnews)

    # def test_detail_news_using_detail_news_template(self):
    #     response = Client().get('/news/detail/<int:pk>')
    #     self.assertTemplateUsed(response, 'detailnews.html') 

    def test_detail_news(self):
        new_news = News.objects.create(
            news_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_program = Program.objects.create(
            program_name = "baksos", 
            summary = "menjajah desa",
            description = "halo",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_donatur = Donatur2.objects.create(
            donatur_name = "Arif Dermawan", 
            donatur_showable = True,
            total_money = 1000000)
        new_donatur.save()
        new_program.donatur.add(new_donatur)
        new_news.program.add(new_program)

        response = Client().get('/news/detail/1/')
        self.assertEqual(response.status_code, 200)
