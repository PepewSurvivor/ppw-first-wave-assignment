from django.urls import path
from news.views import news, detailnews

app_name = 'news'

urlpatterns = [
    path('', news, name = 'news'),
    path('detail/<int:pk>/', detailnews, name = 'detailnews'),
]