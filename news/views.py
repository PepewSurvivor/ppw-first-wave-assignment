from django.shortcuts import render
from main.models import News

def news(request):
	news_data_all = News.objects.all()[:]
	return render(request, 'news.html', {'news_data_all':news_data_all})

def detailnews(request,pk):
	news_data = News.objects.get(pk=pk)
	return render(request, 'detailnews.html', {'news_data':news_data})

