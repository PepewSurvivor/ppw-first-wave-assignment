from django.shortcuts import render
from main.models import Program

def dashboard(request):
	total = {}
	prog = Program.objects.exclude(program_name = 'Donasi Global')
	try:
		glob = Program.objects.get(program_name = 'Donasi Global')
	except :
		glob = None
	temp = 0
	for program in prog:
		temp += int(program.get_all_donation())   
	return render(request, 'dashboard.html', {'donasi':prog, 'total':temp, 'glob':glob})

def detail(request,pk):
	prog = Program.objects.get(pk=pk)
	prog_sel = prog.donatur.all()
	donatur = Program.objects.all()
	return render(request, 'detail.html', {'prog' : prog, 'donatur' : donatur, 'prog_sel' : prog_sel})