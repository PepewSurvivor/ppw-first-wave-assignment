from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from main.models import *
import dashboard.views as views

class DashboardUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        new_program = Program.objects.create(
            program_name = "Donasi Global",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")
        
          # add the donatur
        new_donatur1 = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin",
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")
        new_donatur2 = Donatur2.objects.create(
            donatur_name = "Arif Dermawan",
            donatur_email = "arif.dermawan@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")
        new_donatur1.save()
        new_donatur2.save()
        new_program.donatur.add(new_donatur1, new_donatur2)
        
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_using_dashboard_func(self):
        
        found = resolve('/dashboard/')
        self.assertEqual(found.func, views.dashboard)

    # def test_is_donasi_global_is_none(self):
    #     new_program = Program.objects.create(
    #         program_name = "banjir pekalongan",
    #         summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    #         description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    #         photo = "https://files.catbox.moe/kil8y5.jpg")
    #     response = dashboard(request)
    #     html_response = response.content.decode('utf8')
    #     if(new_program == None):
    #         self.assertIn('Tidak Ada Donasi', html_response)
    #         self.assertFalse(new_program == "Donasi Global")

    def test_dashboard_using_dashboard_template(self):
        new_program = Program.objects.create(
            program_name = "Donasi Global",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")
        
        response = Client().get('/dashboard/')
        self.assertTemplateUsed(response, 'dashboard.html')

    def test_detail_prog(self):
        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_donatur1 = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin",
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")
        new_donatur2 = Donatur2.objects.create(
            donatur_name = "Arif Dermawan",
            donatur_email = "arif.dermawan@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")
        new_donatur1.save()
        new_donatur2.save()
        new_program.donatur.add(new_donatur1, new_donatur2)

        response = Client().get('/dashboard/detail/1/')
        self.assertEqual(response.status_code, 200)

    def test_if_donasi_global_none(self):

        new_program = Program.objects.create(
            program_name = "banjir pekalongan",
            summary = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            description = "tralalaaaaaaaaaaaaaaaaaaaaaaaaaaatralalaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            photo = "https://files.catbox.moe/kil8y5.jpg")

        new_donatur1 = Donatur2.objects.create(
            donatur_name = "Clouddian Fazalmuttaqin",
            donatur_email = "clauddian9@gmail.com",
            donatur_showable = True,
            total_money = 1000000,
            program_name = "bantu aku")

        new_donatur1.save()
        new_program.donatur.add(new_donatur1)
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn("",html_response)




