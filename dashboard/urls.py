from django.urls import path
from dashboard.views import *

app_name = 'dashboard'
urlpatterns = [
	path('detail/<int:pk>/', detail, name = 'detail'),
    path('', dashboard, name = 'dashboard'),
]