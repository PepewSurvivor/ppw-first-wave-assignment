var nameIsValid = false;
var numberIsValid = false;

function nameValidation() {
    var inputDonaturName = $('#inputDonaturName').val()
    if (inputDonaturName.length > 0 && inputDonaturName.length <= 100) {
        nameIsValid = true;
        $('#invalid-name').text("");
        $('#inputDonaturName').removeClass('is-invalid');
        $('#inputDonaturName').addClass('is-valid');
    } else {
        nameIsValid = false;
        $('#invalid-name').text("Input kosong atau nama anda melebih 100 karakter");
        $('#invalid-name').css({"color":"white","text-align":"left"});
        $('#inputDonaturName').removeClass('is-valid');
        $('#inputDonaturName').addClass('is-invalid');
    }
    buttonIsVisible()
}

function numberValidation() {
    var inputDonationTotal = $('#inputDonationTotal').val()
    if (inputDonationTotal.length > 0 && parseInt(inputDonationTotal) <= 1000000000) {
        numberIsValid = true;
        $('#invalid-total').text("");
    } else {
        numberIsValid = false;
        $('#invalid-total').text("Nominal yang anda masukkan kosong atau melebihi 1 miliar");
        $('#invalid-total').css({"color":"white","text-align":"left"});
    }
    buttonIsVisible()
}

function buttonIsVisible() {
    if (numberIsValid && nameIsValid) {
        $('#submit').removeClass('disabled');
    } else if (!$('#submit').hasClass('disabled')) {
        $('#submit').addClass('disabled');
    }
}

$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
                }
            }
        }
        return cookieValue;
    }
    
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $(document).on('submit', '#new_donation',function(e) {
        if(!$('#submit').hasClass('disabled')) {
            var isHidden = "0";
            if ($('#isHidden').prop("checked")) isHidden = "1";
            console.log(isHidden)
            e.preventDefault();
            $.ajax({
                type:'POST',
                url:'/donation_form/donation_add/',
                data:{
                    program_name:$('#inputProgramName').val(),
                    is_hidden:isHidden,
                    donatur_name:$('#inputDonaturName').val(),
                    total_money:$('#inputDonationTotal').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                },
                sucess:function() {
                }
            });
            $('#inputDonaturName').val("")
            $('#inputDonationTotal').val("")
            $('#isHidden').prop("checked", false);
            alert("Donasi anda telah tersalurkan!");
            nameIsValid = false;
            numberIsValid = false;
        } else {
            return false;
        }
    });
});